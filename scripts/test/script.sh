#!/bin/bash

echo CI_PROJECT_DIR $CI_PROJECT_DIR
pwd
cd hello
ls -la
go build -v
./hello
go list -m all
ls $(go env GOCACHE) || true
